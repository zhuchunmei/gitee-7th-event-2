  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/174156_8c1a9cdd_1899542.png "悬赏令.png")  
**[活动主会场 >>](https://oschina.gitee.io/gitee-7th/?utm_source=gitee-event-2):sparkles:** 
  
#### 活动介绍
通过解决开源软件 issue（修改bug 、新增功能等）参与开源贡献，解决不同难度的 issue 可获得相应礼品。  
  
#### 主办方  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/180153_692e0ecf_1899542.png "logo.png")
  
#### 活动流程
1、题库征集：开源软件作者选取软件中需要协助的 issue 提交至活动题库（如 bug 、功能等）  
[提交 issue（点击提交）](https://jinshuju.net/f/4i0NvK)      
  
2、题库生成：Gitee 统一审核 issue ，收录符合条件的 issue 至活动题库（ 题库将按 issue 难度划分（困难、中等、简单），题库集满为止 ）   
  
3、题库解决：参赛者通过完成待解决的 issue 参与开源贡献，解决不同难度的 issue 可获得与难度相应的的奖品  
时间：6月1日 - 6月22日  
  
4、题库审核: 开源软件作者统一审核活动提交的 PR   
时间：6月23日 - 6月24日
  
#### 活动题库：待解决的 issue 

|  issue 难度   |   征集个数  |  参赛 issue   | issue 所属软件 |  完成奖励 |
| --- | --- | --- |  ---| ---|
|  困难   |  3   |   [点击查看](https://gitee.com/Selected-Activities/gitee-7th-event-2/blob/master/issue-difficult.md)  | Apache DolphinScheduler、RT-Thread、SOFAJRaft|  Apple ipad / SONY 数码相机/ 2000 元京东购物卡  |
|  中等   |  10   |  [点击查看](https://gitee.com/Selected-Activities/gitee-7th-event-2/blob/master/issue-medium.md)   | Bootstrap Blazor、OpenAuth.Core、Jenkins CLI、Dataway、Apache DolphinScheduler、SOFABolt、RT-Thread、kkfileview | 罗技（G）机械键盘/SKG 颈椎按摩器/500 元京东卡  |
|  简单   |  20   |  [点击查看](https://gitee.com/Selected-Activities/gitee-7th-event-2/blob/master/issue-simple.md)   | RT-Thread | Gitee T恤 / 100元京东购物卡 |

### 参与方式
**开源作者：**   
1. [提交 issue（点击提交）](https://jinshuju.net/f/4i0NvK)  
开源作者可以选取软件中需要他人协助的 issue 提交进题库
2. 审核 issue PR: 6 月 23 日，对参与本次活动的 issue 提交的 Pull Request 进行审核。

**参赛者：**  :point_right: [帮助文档](https://gitee.com/help/categories/42)  
1. 在待解决的 issue 中选择一个或多个，解决它。
2. 解决 issue 后，再 issue 所属仓库提交 Pull Request 并复制 issue ID 至 PR 描述，点击“提交审核”按钮，注意：请备注【开源贡献活动】关键字（注：在 issue 所属仓库内提交即可，无需在本仓库提交）
3. 开源软件作者会在 6 月 23 日 审核提交的 Pull Request，Pull Request审核通过的小伙伴即可获得该 issue 对应的奖品

####  :gift: 活动奖励
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/104140_e9912ec0_1899542.png "修改奖品图片-奖品设置.png")
 