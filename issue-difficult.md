#### 困难级 issue :fa-star: :fa-star: :fa-star: 
解决以下任意一个 issue 即可获得：  
![输入图片说明](https://images.gitee.com/uploads/images/2020/0603/162908_b52f3fa9_1899542.png "屏幕截图.png")       
  
#### 参与方式
1. 点击 issue 链接查看详情
2. 解决 issue 后，在 issue 所属仓库提交 Pull Request 并复制 issue ID 至 PR 描述，点击“提交审核”，注意：请备注【开源贡献活动】关键字（注：在 issue 所属仓库内提交即可，无需在本活动仓库再次提交）
3. 6月23日，开源软件作者/团队统一审核提交的 Pull Request，Pull Request 审核通过的小伙伴即可获得该 issue 对应的奖品  
  
| 复杂度  | 类型  |  issue 描述 | issue 链接  | 所属软件 |
|---|---|---|---|---|
| 困难  |  新功能 | 增加imx6ull BSP  |  https://gitee.com/rtthread/rt-thread/issues/I1I3VP | RT-Thread  |
| 困难  | 改进  | 当前 LogStorage 的实现，采用 index 与 data 分离的设计，我们将 key 和 value 的 offset 作为索引写入 rocksdb，同时日志条目（data）写入 Segnemt Log，因为使用 SOFAJRaft 的用户经常也使用了不同版本的 rocksdb，这就要求用户不得不更换自己的 rocksdb 版本来适应 SOFAJRaft， 所以我们希望做一个改进：移除对 rocksdb 的依赖，构建出一个纯 java 实现的索引模块。  | https://gitee.com/sofastack/sofa-jraft/issues/I1I9C8  |  SOFAJRaft |
| 困难  | 新功能  |  1、支持sql节点和etl节点自动分析表之间的依赖关系，通过解析sql的select表和insert表实现，其他节点可以手工维护insert表（如果存在这种需求）2、前端通过依赖检测开关控制master在任务调度时是否进行依赖解析 3、master server自动注入依赖节点，运行时根据依赖关系生成虚拟依赖节点，不修改工作流定义数据，仅针对定时调度上线的工作流进行依赖分析 4、生成的依赖节点设置默认失败重试次数，比如每5分钟检测一下 5、打开依赖解析开关后，节点间不再需要手工连线，master根据依赖关系的顺序进行节点的调度  | https://gitee.com/dolphinscheduler/DolphinScheduler/issues/I1I5C9  | Apache DolphinScheduler |  

[查看更多悬赏中的 issue >>](https://gitee.com/Selected-Activities/gitee-7th-event-2)
